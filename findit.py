#!/usr/bin/python3
'''
Version 2.1 - add -f for full path search
Version 2 - added -o -t -m'''
import threading
import os
import sys
import fnmatch
from pathlib import Path
import argparse
import re
import stat
import datetime


class style():
  BLACK = '\033[30m'; RED = '\033[31m'; GREEN = '\033[32m'; YELLOW = '\033[33m'
  BLUE = '\033[34m'; MAGENTA = '\033[35m'; CYAN = '\033[36m'; WHITE = '\033[37m'; GRAY = '\033[90m'
  RED2 = '\33[91m'; GREEN2 = '\33[92m'; YELLOW2 = '\33[93m'; BLUE2 = '\33[94m'
  MAGENTA2 = '\33[95m'; CYAN2 = '\33[96m'; WHITE2  = '\33[97m'
  UNDERLINE = '\033[4m'; RESET = '\033[0m'

def clear_style():
  style.BLACK = ''; style.RED = ''; style.GREEN = ''; style.YELLOW = ''
  style.BLUE = ''; style.MAGENTA = ''; style.CYAN = ''; style.WHITE = ''; style.GRAY = ''
  style.RED2 = ''; style.GREEN2 = ''; style.YELLOW2 = ''; style.BLUE2 = ''
  style.MAGENTA2 = ''; style.CYAN2 = ''; style.WHITE2  = ''
  style.UNDERLINE = ''; style.RESET = ''


def msg(msg1, clr=''):
  print(f'{clr}{msg1}{style.RESET}')


def get_files(sourceDir, maxdepth):
  """Get a list of file"""
  retset = set()
  if (maxdepth < 1 ):
    maxdepth = None
  #for sdir in { Path(x).resolve() for x in sourceDir if Path(x).is_dir() }:
  for sdir in { Path(x) for x in sourceDir if Path(x).is_dir() }:
    depth = maxdepth
    if (depth and depth == 1):
      for filename in Path(sdir).glob('*'):
        if (filename.exists() == True):
          retset.add(Path(filename))
      retset.add(Path('.'))
      retset.add(Path('..'))
    else:
      top_pathlen = len(str(sdir)) + len(os.path.sep)
      for root, dirs, files in os.walk(sdir):
        dirlevel = root[top_pathlen:].count(os.path.sep) + 1
        if (depth and dirlevel >= depth):
          dirs[:] = []
        else:
          for basename in files:
            retset.add(Path(root) / basename)

  return (retset)


def filter_list(flist, args):
  if (args.case == True):
    flags = 0
  else:
    flags = re.IGNORECASE
  retset = set(flist)
  for ereg in args.eregs:
    flist = retset
    retset = set()
    for file in flist:
      if (args.full == True):
        f = f"{str(file)}"
        eereg = ereg
      else:
        f = f"{file.name}"
        eereg = ereg
        if (str(file) == '.'):
          f = f"{str(file)}"
      x = re.search(eereg, f, flags = flags)
      if (x is not None):
        retset.add(file)

  return(retset)

def sizeof_fmt(num, suffix="B"):
    for unit in ["", "K", "M", "G", "T", "P", "E", "Z"]:
        if abs(num) < 1024.0:
            return f"{num:3.1f} {unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f} Y{suffix}"


def print_args(args):
  '''
  print out the args'''

  print (args)

def get_file_info(filelist):

  dic_file_info={}
  owner_max_len=0
  group_max_len=0
  size_max_len=0
  for x in filelist:
    temp_dic={}
    try:
      if (x.exists() == False):
        continue 
    except:
      continue
    if (x.is_symlink() == True):
      fstat = x.lstat()
    else:
      fstat = x.stat()
    imode = f'{stat.filemode(fstat.st_mode)}'
    isize = f'{sizeof_fmt(fstat.st_size)}'
    imtime_date = f"{datetime.datetime.fromtimestamp(fstat.st_mtime).strftime('%m/%m/%Y')}"
    imtime_time = f"{datetime.datetime.fromtimestamp(fstat.st_mtime).strftime('%I:%M.%S %p')}"
    try:
      iowner = f'{x.owner()}'
      iowner_max = len(iowner)
    except:
      iowner = f'{fstat.st_uid}'
      iowner_max = len(iowner)

    try: 
      igroup = f'{x.group()}'
      igroup_max = len(igroup)
    except:
      igroup = f'{fstat.st_gid}'
      igroup_max = len(igroup)
    
    if (x.is_dir() == True):
      idir = f'{str(x)}{os.sep}'
      iname = ''
    else:
      idir = f'{x.parent}{os.sep}'
      iname = f'{x.name}'
    if (iowner_max > owner_max_len):
      owner_max_len = iowner_max
    if (igroup_max > group_max_len):
      group_max_len = igroup_max
    if (len(isize) > size_max_len):
      size_max_len = len(isize)
    #create dictionary
    temp_dic['dir'] = idir 
    temp_dic['name'] = iname
    temp_dic['mode'] = imode
    temp_dic['size'] = isize
    temp_dic['owner'] = iowner
    temp_dic['owner_len'] = iowner_max
    temp_dic['group'] = igroup
    temp_dic['group_len'] = igroup_max
    temp_dic['mtime_date'] = imtime_date
    temp_dic['mtime_time'] = imtime_time
    dic_file_info[f'{str(x)}'] = temp_dic
  temp_dic = {}

  temp_dic['owner_max'] = owner_max_len
  temp_dic['group_max'] = group_max_len
  temp_dic['size_max'] = size_max_len
  dic_file_info['--:--MAX--:--'] = temp_dic

  return(dic_file_info)

def highlight_match(regxs,s, casesensitivity,colr):
  colourStr = style.RED
  resetStr = style.RESET

  for regx in regxs:
    if (regx == '.'):
      continue
    if (casesensitivity == False):
      regx = regx.lower()
      s = s.lower()
    lastMatch = 0
    formattedText = ''
    for match in re.finditer(regx, s):
      start, end = match.span()
      formattedText += s[lastMatch: start]
      formattedText += colourStr
      formattedText += s[start: end]
      formattedText += resetStr + colr
      lastMatch = end
    formattedText += s[lastMatch:]
    s = formattedText
  return(s)

def print_results(dic_file_info, args):
  amode = False
  asize = False
  aowner = False
  agroup = False
  adate = False
  atime = False
  ainfo = False
  if (args.long == True):
    amode = True
    asize = True
    aowner = True
    agroup = True
    adate = True
    atime = True
    ainfo = True
  if (args.LONG == True):
    amode = False
    asize = False
    aowner = False
    agroup = False
    adate = False
    atime = False
    ainfo = False

  if (args.permission == True):
    amode = True
  if (args.size == True):
    asize = True
  if (args.owner == True):
    aowner = True
  if (args.group == True):
    agroup = True
  if (args.date == True):
    adate = True
  if (args.time == True):
    atime = True
  if (args.info == True):
    ainfo = True

  if (args.PERMISSION == True):
    amode = False
  if (args.SIZE == True):
    asize = False
  if (args.OWNER == True):
    aowner = False
  if (args.GROUP == True):
    agroup = False
  if (args.DATE == True):
    adate = False
  if (args.TIME == True):
    atime = False
  if (args.INFO == True):
    ainfo = False

  if (len(dic_file_info) <= 1):
    msg('Nothing found.', style.BLUE)
  else:
    owner_max = dic_file_info['--:--MAX--:--']['owner_max']
    group_max = dic_file_info['--:--MAX--:--']['group_max']
    owner_group_max = owner_max + group_max + 1
    size_max = dic_file_info['--:--MAX--:--']['size_max']

    del dic_file_info['--:--MAX--:--']

    dir_set = set()

    out_size = 0 
    for x in sorted(dic_file_info, key = lambda k:(dic_file_info[k]['dir'], dic_file_info[k]['name'])):
      y = dic_file_info[x]
      if (args.name == True):
        #if name is blank then it must be a dir, so print out dir
        if (y["name"] == ''):
          name = f'{style.YELLOW}{highlight_match(args.eregs,y["dir"], args.case, style.YELLOW)}'
        else:
          name = f'{style.YELLOW2}{highlight_match(args.eregs,y["name"], args.case, style.YELLOW2)}'
      else:
        if (args.absolute == True):
          dir = y["dir"]
          adir = str(Path(dir).resolve())
          dirsplit = []
          if (dir == './'):
            dirsplit.append(adir)
          else:
            dirsplit = adir.rsplit(dir[0:len(dir)-1], 1)
          if (len(dirsplit) == 1):
            name = f'{style.YELLOW}{dirsplit[0]}{os.sep}{style.YELLOW2}{highlight_match(args.eregs,y["name"], args.case, style.YELLOW2)}'
          else:
            sdir = highlight_match(args.eregs, dir, args.case, style.YELLOW)
            name = f'{style.YELLOW}{dirsplit[0]}{sdir}{style.YELLOW2}{highlight_match(args.eregs,y["name"], args.case, style.YELLOW2)}'
           
          #name = f'{style.YELLOW}{highlight_match(args.eregs, str(Path(y["dir"]).resolve()), args.case, style.YELLOW)}{os.sep}{style.YELLOW2}{highlight_match(args.eregs,y["name"], args.case, style.YELLOW2)}'
        else:
          dir = y["dir"]
          if (dir == f'.{os.sep}' and y["name"] != ''):
            dir = ''
          name = f'{style.YELLOW}{highlight_match(args.eregs, dir, args.case, style.YELLOW)}{style.YELLOW2}{highlight_match(args.eregs, y["name"], args.case, style.YELLOW2)}'
      
      mode = f'{style.GREEN}{y["mode"]} '
      size = f'{style.GREEN2}{y["size"]:>{size_max}} '
      owner = f'{style.CYAN}{y["owner"]:>{owner_max}} '
      group = f'{style.CYAN2}{y["group"]:>{group_max}} '
      owner_group = f'{style.CYAN}{y["owner"]:>{owner_max}}:{style.CYAN2}{y["group"]:<{group_max}}'
      fowner = f'{style.GREEN2}{owner_group:^{owner_group_max}} '
      fdate = f'{style.BLUE}{y["mtime_date"]} '
      ftime = f'{style.BLUE2}{y["mtime_time"]} '
     # datefmt = f'{style.BLUE}{y["mtime_date"]} {style.BLUE2}{y["mtime_time"]} '

      out=''
      out_size = 0
      if (amode == True):
        out = out + mode
        out_size += 11
      if (asize == True):
        out = out + size
        out_size += size_max + 1
      if (aowner == True and agroup == True):
        out = out + fowner
        out_size += owner_max + group_max + 2
      elif (aowner == True):
        out = out + owner
        out_size += owner_max + 1
      elif (agroup == True):
        out = out + group
        out_size += group_max + 1
      if (adate == True): 
        out = out + fdate
        out_size +=  11
      if (atime == True):
        out = out + ftime
        out_size += 12
        
      dir_set.add(y['dir'])
      
      msg(out + name) 

    files =len([x['name'] for x in dic_file_info.values() if x['name'] != ''])
    dirs = len(dir_set)

    if (ainfo == True):
      info = f'Files: {files}     Dirs: {dirs}'
      if (out_size < len(info)):
        out_size = len(info)
      else:
        out_size -= 1
      msg( u'\u2500' * out_size)
      msg(info)

      # if (args.long == True):
      #   mode = f'{style.CYAN}{y["mode"]} '
      #   size = f'{style.GREEN}{y["size"]:>{size_max}} '
      #   owner = f'{y["owner"]:>{owner_max}}:{y["group"]:<{group_max}}'
      #   fowner = f'{style.GREEN2}{owner:^{owner_group_max}} '
      #   datefmt = f'{style.BLUE}{y["mtime_date"]} {style.BLUE2}{y["mtime_time"]} '
      #   msg(f'{mode}{size}{fowner}{datefmt}{name}') 
      # else:
      #   msg(name,style.YELLOW)

def print_results2(filelist, args):
  if (len(filelist) == 0):
    msg('Nothing found.', style.BLUE)
  else:
    for x in sorted(filelist):
      if (args.name == True):
        name = f'{style.YELLOW2}{x.name}'
      else:
        name = f'{style.YELLOW}{x.parent}{os.sep}{style.YELLOW2}{x.name}'
      if (args.long == True):
        fstat = x.stat()
        mode = f'{style.CYAN}{stat.filemode(fstat.st_mode)} '
        size = f'{style.GREEN}{sizeof_fmt(fstat.st_size):>9} '
        owner = f'{x.owner():.9}:{x.group():.9}'
        fowner = f'{style.GREEN2}{owner:>19} '
        datefmt = f"{style.BLUE}{datetime.datetime.fromtimestamp(fstat.st_mtime).strftime('%m/%m/%Y')} {style.BLUE2}{datetime.datetime.fromtimestamp(fstat.st_mtime).strftime('%I:%M:%S %p')} "
        msg(f'{mode}{size}{fowner}{datefmt}{name}') 
      else:
        msg(name,style.YELLOW)

def main():
  parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description=f'This is bascially a simple ls and find built into one.\n' +
                f'Run it with no arguments is the same as doing -e "." -m1.\n' +
                f'The -e "." mean match everything, -m1 just show one level, so the current dir.\n' +
                f'You can pass it multiple dirs, and with the -e you can pass it mutiple regex.\n' +
                f'If you wanted to search for a file that has the name "hello" in it that might be located in 2 dirs:\n' +
                f'findit.py ~/Downloads ~/Documents -e "hello"\n' +
                f'If you wanted to find the words "hello" and "hi" you would do:\n' +
                f'findit.py ~/Downloads ~/Documents -e "hello|hi"\n' 
                ,
    epilog=""
  )
  #parser.add_argument("Dirs", help="List of directoies to search", nargs='+')
  parser.add_argument("Dirs", help="List of directoies to search, default is the current dir", nargs='*', default='.')
  parser.add_argument('-a', '--absolute', help='Expand dir.', action='store_true')
  parser.add_argument('-c', '--case', help='Case sensitivity.', action='store_true')
  parser.add_argument('-i', '--info', help='Display info footer.', action='store_true')
  parser.add_argument('-I', '--INFO', help='Do not isplay info footer.', action='store_true')

  #parser.add_argument('-e', '--eregs', help="Mutile regular expressions.", nargs='+', required=True)
  parser.add_argument('-e', '--eregs', help="Mutile regular expressions, default is '.' match everything", nargs='+', default='.')
  parser.add_argument('-l', '--long', help="Long output, same as -i -m -s -o -g -d -t.", action='store_true')
  parser.add_argument('-L', '--LONG', help="Turn off -i -m -s -o -g -d -t.", action='store_true')
  parser.add_argument('-f', '--full', help='Search on full path dir + name', action='store_true')
  parser.add_argument('-n', '--name', help='Display name without path.', action='store_true')
  parser.add_argument('-p', '--permission', help='Display mode (permissions) of a file.', action='store_true')
  parser.add_argument('-P', '--PERMISSION', help='Do not display mode (permissions) of a file.', action='store_true')
  parser.add_argument('-s', '--size', help='Display size of a file.', action='store_true')
  parser.add_argument('-S', '--SIZE', help='Do not display size of a file.', action='store_true')
  parser.add_argument('-o', '--owner', help='Display owner of a file.', action='store_true')
  parser.add_argument('-O', '--OWNER', help='Do not display owner of a file.', action='store_true')
  parser.add_argument('-g', '--group', help='Display group user of a file.', action='store_true')
  parser.add_argument('-G', '--GROUP', help='Do not display group user of a file.', action='store_true')
  parser.add_argument('-d', '--date', help='Display modified date of a file.', action='store_true')
  parser.add_argument('-D', '--DATE', help='Do not modified display date  a file.', action='store_true')
  parser.add_argument('-t', '--time', help='Display modified time of file.', action='store_true')
  parser.add_argument('-T', '--TIME', help='Do not display modified time of a file.', action='store_true')
  parser.add_argument('-m', '--maxdepth', type=int, help='How many directory levels to show.  If not set then show all.', default=-1)
  parser.add_argument('--COLOR', help='Do not display color.', action='store_true')
  parser.add_argument('--color', help='Display color.', action='store_true')
  parser.add_argument('--version', action='version', version=(f'%(prog)s version: {__version__}'), help = 'show the version number and exit')

  args = parser.parse_args()

  #print_args(args)

  if (((os.fstat(0) != os.fstat(1)) or args.COLOR == True ) and args.color == False):
    clear_style()

  for x in args.eregs:
    try:
      tst = re.compile(x)
    except:
      msg(f'Bad regex: {x}', style.RED2)
      msg('Please try again...', style.RED2)
      exit()

  mdepth = 0
  if (args.eregs == '.' and args.maxdepth == -1):
    mdepth = 1
  else:
    mdepth = args.maxdepth

  #retset = get_files(args.Dirs, args.maxdepth)
  retset = get_files(args.Dirs, mdepth)

  retset = filter_list(retset, args)

  dic_file_info = get_file_info(retset)

  print_results(dic_file_info, args)



if __name__ == "__main__":
  __version__ = '2.1'
  main()
