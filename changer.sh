#!/bin/bash
# Author: Matthew Weber AKA The Linux Cast
# Version: 0.1 
# Description: This script allows you to change between several DWM Rices.
# Dependencies: pkexec, sed, git, polkit, rofi
# WARNING: In order for this script to work, you will eventually be asked for a root password. In order for this to happen, you will need a polkit agent installed and running. 

##########################################################################

dthemedir=$HOME/.config/suckless/dwm/themes
hfile=$HOME/.config/suckless/dwm/config.h
shfile=$HOME/.config/suckless/slstatus/config.h
ddir=$HOME/.config/suckless/dwm
sdir=$HOME/.config/suckless/slstatus
sthemedir=$HOME/.config/suckless/dwm/themes


declare -a options=(
    "dracula"
    "gruv"
    "monokai"
    "ocean"
    "papercolor"
    "moonfly"
    "onedark"
    "purple"
    "darkblue"
    "quit"
)

choice=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -l 20 -p 'Themes')

if [[ "$choice" == quit ]]; then
    echo "Program Terminated" && exit 1
fi

if [ -f "$hfile" ]; then
    rm $hfile
fi

if [ -f "$shfile" ]; then
    rm $shfile
fi

if [ "$choice" ]; then
    cp "$dthemedir"/"$choice".h "$dthemedir"/theme.h && make -C "$ddir" && pkexec make install -C "$ddir" && cp "$sthemedir"/"$choice".h "$sthemedir"/theme.h && make -C "$sdir" && pkexec make install -C "$sdir" 

fi


