#!/bin/sh
path="$HOME/myrepo/i3/themes"
ptheme="$HOME/myrepo/polybar/themes"
mod="$HOME/myrepo/polybar/themes/modules"

options="\
doom-one
dracula
catppuccin
gruvbox
gruvbox_powerline
monokai-pro
nord
ocean
tomorrow-night
onedark
papercolor
onedark
tokyonight
moonfly
sonokai
quit"

choice=$(printf '%s\n' "${options}" | rofi -dmenu -i -l 20 -p 'Themes')

if ! [ "${choice}" ] || [ "${choice}" = "quit" ]; then
    printf 'No theme chosen\n' >&2
    exit 1
fi

cp "$path/${choice}.conf" "${path}/theme.conf"
cp "${ptheme}/${choice}.ini" "${ptheme}/theme.ini"
cp "${mod}/${choice}.ini" "${ptheme}/modules.ini"
i3-msg restart
