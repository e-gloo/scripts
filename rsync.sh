#!/bin/bash
date=$(date +%Y.%m.%d-%H.%M.%S)
budir="/artemis/Backups/2022/fedora/"
excfile="$HOME/media/Documents/exclude.txt"

mkdir $budir/$date

rsync -av --exclude 'media/Music' --exclude 'Downloads/isos' --exclude '.local/share/Steam' --exclude '.cache' --exclude '.var' --exclude 'pCloudDrive' /home "$budir"/"$date"
#rsync -av --exclude-from '/home/drmdub/media/Documents/rs' /home "$budir"/"$date"

notify-send -h string:fgcolor:#ff4444 "back up running"
