#!/bin/bash

# This script provides a scratchpad for bspwm
# Written by Bjorn

# Get PID if scratchpad is running
running=$(ps -ef | awk '/alacritty --class scratchpad-htop/ && !/awk/ 
{print $2}')

if [ -z "$running" ]; then
    # Not running, so starting
    alacritty --class scratchpad-htop,scratchpad-htop --title 
Scratchpad-htop -e htop
else
    # Running
    wid="$(xdotool search --pid "$running" | head -n1)"

    if [ -n "$(bspc query -T -n "$wid" | tr ',' '\n' | awk '/true/ && 
/hidden/')" ]; then
        # Hidden, so bring to focused desktop, unhide and focus the window
        bspc node "$wid" -d focused --flag hidden=off -f
    elif [ -n "$(bspc query -T -n "$wid" | tr ',' '\n' | awk '/false/ 
&& /shown/')" ]; then
        # Not hidden, but not on current desktop, so bring to current 
desktop
        bspc node "$wid" -d focused -f
    else
        # Not hidden and shown on current desktop, so hide
        bspc node "$wid" --flag hidden=on
    fi
fi


