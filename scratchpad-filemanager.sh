#!/bin/sh

#Run this script from a keybinding in sxhkd - needs to be executable


#The below rule placed in bswmrc but can be here.
#bspc rule -a st-256color:file_manager state=floating sticky=on rectangle="1800x900+60+90"

#Set variable for command st shell running ranger filemanager
file_manager_cmd="alacritty -t file_manager --class "sc,sc" -e  ranger"

#Check if st terminal with ranger is running and run it if not
if ! pgrep -f "alacritty -t file_manager --class "sc,sc" -e ranger" > /dev/null
then
	$file_manager_cmd &
#if file manager running then toggle on off - need xdo for this.
else
	window_id=$(xdo id -n file_manager)
	bspc node $window_id --flag hidden --focus $window_id

fi	


