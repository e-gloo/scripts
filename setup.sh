#!/bin/bash

myrepo="$HOME/myrepo"
gitthings="$HOME/Downloads/gitthings"
scripts="$HOME/Downloads/gitthings/scripts"

#Installing Dependencies
echo "Installing Dependencies"
sudo pacman -S i3-gaps rofi kdenlive zim obs-studio sxhkd xclip clipmenu krusader kio-extras discord alacritty ranger dunst mpd ncmpcpp mpv neofetch qutebrowser pulsemixer zsh git neovim flatpak && paru -S polybar picom-ibhagwan-git nerd-fonts-complete feh ueberzug

#Download Dots
echo "Downloading Dotfiles"
cd && git clone https://gitlab.com/thelinuxcast/my-dots.git

mv my-dots myrepo

cd $myrepo
ln -s $HOME/myrepo/i3 ~/.config
ln -s $HOME/myrepo/polybar ~/.config
ln -s $HOME/myrepo/alacritty ~/.config
ln -s $HOME/myrepo/zsh ~/.config
ln -s $HOME/myrepo/dunst ~/.config
ln -s $HOME/myrepo/mpd ~/.config
ln -s $HOME/myrepo/ncmpcpp ~/.config
ln -s $HOME/myrepo/mpv ~/.config
#rm -r ~/.config/neofetch && ln -s $HOME/myrepo/neofetch ~/.config
ln -s $HOME/myrepo/picom ~/.config
ln -s $HOME/myrepo/ranger ~/.config
ln -s $HOME/myrepo/qutebrowser ~/.config
ln -s $HOME/myrepo/rofi ~/.config

cd $gitthings && git clone https://gitlab.com/thelinuxcast/scripts.git
cd $scripts
sudo cp *.sh weather.py /usr/local/bin

cd $HOME/.config
git clone https://gitlab.com/thelinuxcast/nvim.git
